#!/bin/bash

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR
echo -en "DIR: $DIR"

# Write the speedtest results to file, then send the file to heartbeat.

speedtest=$(python $DIR/commands/speedtest.py)
echo -en "$speedtest\n\n" > $DIR/speedtest.report

python /root/heartbeat/heartbeat.py "file:$DIR/speedtest.report"

