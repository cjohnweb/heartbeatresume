#!/bin/bash


if [ ! -z "$1" ]; then
	time="$1"
fi

start=$(date +%s)

while true; do

	slp=0.05

	echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr0/brightness 
	sleep $slp
	echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr0/brightness 

        echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr1/brightness 
        sleep $slp
        echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr1/brightness 

        echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr2/brightness 
        sleep $slp
        echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr2/brightness 

        echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr3/brightness 
        sleep $slp
        echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr3/brightness 

        echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr2/brightness 
        sleep $slp
        echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr2/brightness 

        echo "1" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr1/brightness 
        sleep $slp
        echo "0" > /sys/devices/ocp.3/gpio-leds.8/leds/beaglebone:green:usr1/brightness 


	if [ ! -z ${time+x} ]; then

	        now=$(date +%s)
        	total=$(($now-$start))

		echo -en "Total: $total Time: $time\n"
		if [ "$total" -ge "$time" ]; then
			break
		fi
	fi

	fn="../controls/indication.cmd"

	if [ -f $fn ];
	then

		controlbreak=$(cat $fn)

		if [ "$controlbreak" == "break" ]; then
			rm $fn
			break
		fi

	fi

done
