#!/bin/bash  

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

sudo chmod 600 ~/.ssh/id_rsa
HOSTNAME=$(cat /etc/hostname)
echo -en "127.0.0.1\tlocalhost\n127.0.1.1\t$HOSTNAME\n" >  /etc/hosts
sudo chmod 600 ~/.ssh/id_rsa
sudo chmod 600 ~/.ssh/id_rsa.pub
sudo chmod 644 ~/.ssh/known_hosts
sudo chmod 755 ~/.ssh


