#!/bin/bash
# Add heartbeat to crontabs

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

# Save current Cron to file
crontab -l > $DIR/crontab.out

python /root/heartbeat/heartbeat.py "file:/root/heartbeat/commands/crontab.out"

