#!/bin/bash

if [ -z "$1" ]; then
	echo -en "This script requires a transaction ID to be passed at the CLI.\n\n"
	python /root/heartbeat/heartbeat.py "The generate-and-sync-new-rsa.bash was run but did not receive a Transaction ID." "$1"
else

	hash=$(sha1sum /root/.ssh/id_rsa)

	echo -en "Clearing out previous rsa_id.\n"

	rm /root/.ssh/*rsa*

	echo -en "Generating new 4096 bit key.\n"

	ssh-keygen -b 4096 -t rsa -f /root/.ssh/id_rsa -q -N "" #"$pwd"

	echo -en "Sending new PubKey to Heartbeat Server.\n"

	newhash=$(sha1sum /root/.ssh/id_rsa)

	python /root/heartbeat/heartbeat.py "file:/root/.ssh/id_rsa.pub" "$1"

	python /root/heartbeat/heartbeat.py "Overwritting existing RSA Key Pair: $hash" "$1"
	python /root/heartbeat/heartbeat.py "Hash of New RSA Key: $newhash" "$1"


	echo -en "Hash of old RSA Key: $hash\n\n"
	echo -en "Hash of New RSA Key: $newhash"

fi








