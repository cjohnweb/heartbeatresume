#!/bin/bash

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

git fetch --all
git reset --hard origin/master
