#!/bin/bash

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

version=`cat version.txt`
new=$((version+1))
echo $new > "version.txt"

rm $DIR/init.hash

git add --all

if [ -z "$1" ]
then
	git commit -m "Project Push"
else
	git commit -m "$1"
fi

git push
