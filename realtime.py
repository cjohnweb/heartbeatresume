#!/usr/bin/python
# encoding=utf8

from time import sleep, time

from heartbeat import heartbeat

hb = heartbeat()

data = dict()

data['timestamp'] = int(time())

while True:
  print "\n\n"
  d = hb.createrequest("01","Heartbeat",hb.createtransaction(),"heartbeat",data)
  hb.docomm(d)
  print "\n\n"
  config = hb.getConfig()
  print "Waiting for "+str(config['frequency'])+" seconds..."
  sleep(float(config['frequency']))
