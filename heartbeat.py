#!/usr/bin/python
# encoding=utf8

# Basic imports 
from time import sleep, time
import subprocess, sys, os # used to get IP, system calls, etc
import json
import random
import socket

# For web support
import httplib
import urllib
import zlib
import base64
from threading import Thread
import pwd # To check if a linux user exists
import traceback # For better error reporting

class heartbeat:

	# * * * * 
	def __init__(self):
		self.debug = True
		self.flag = dict()
		self.dir = os.path.dirname(os.path.realpath(__file__))
		self.transaction = self.createtransaction() # Create the transaction now, and overwrite it if a new transaction is passed in.
		# Read in hearbeat config file
		try:
			self.conf = self.load_conf()
			self.conf['frequency'] = float(self.conf['frequency'])
		except Exception, e:
			print "Error reading hb.conf, loading failsafe config: ", e
			# Fail safe - Hardcoded server values in the event of corrupt configuration, etc
			self.conf = dict()
			self.conf['frequency'] = float(5)
			self.conf['realtime'] = True
			self.conf['servers'] = dict()
			self.conf['servers']['failsafe'] = dict()
			self.conf['servers']['failsafe']['protocol'] = "http"
			self.conf['servers']['failsafe']['url'] = "heartbeatiot.com" # /heartbeat.php
			self.conf['servers']['failsafe']['port'] = "80"
			self.conf['servers']['failsafe']['timeout'] = "10"
			self.conf['servers']['failsafe']['enabled'] = "true"
			self.conf['servers']['failsafe']['commands-allowed'] = "true"
			# Debug config variables
			#if (self.debug == True):
			#print "\n\nHeartbeat Configuration:\n\n", str(self.conf), "\n\n"
			# Show Variables
			#for k,v in self.conf['servers'].items():
			#	print "Here: ", k, "\n"
			#	for kk,vv in v.items():
			#		print "\t",kk, " = ", vv, "\n"

		# Use traceroute or other techniques to determine internet availability?
		# Add commands and functions to change config file settings

		self.state = self.conf

		if __name__ == "__main__":

			# Make sure the real time is running, according to the config file
			if self.conf['realtime'] == "true":
			#x = subprocess.check_output('ps aux | grep "realtime.py" | grep -v "grep"', shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

				ps = subprocess.Popen(['ps','aux'], 
					stdout=subprocess.PIPE,
				)

				grep = subprocess.Popen(['grep', 'realtime.py'],
					stdin=ps.stdout,
					stdout=subprocess.PIPE,
				)

				grepv = subprocess.Popen(['grep','-v','grep'],
					stdin=grep.stdout,
					stdout=subprocess.PIPE,
				)

				end_of_pipe = grepv.stdout

				output = str()

				for line in end_of_pipe:
					output += line.strip()

				if not output:
					FNULL = open(os.devnull, 'w')
					subprocess.Popen(['nohup','python','/root/heartbeat/realtime.py'], stdout=FNULL, stderr=subprocess.STDOUT)

			if len(sys.argv) > 1: # If a message was passed
				code = "02"
				param = sys.argv[1]
				if len(sys.argv) > 2:
					self.transaction = sys.argv[2]

				if(param[0].lower()+param[1].lower()+param[2].lower()+param[3].lower()+param[4].lower() == "file:"):

					filename = param[5:] #5: removes file:  from string
					binary = str()
					
					# RSA public keys come in with a different code so we can catch it and process it differently
					if(filename == "/root/.ssh/id_rsa.pub"):
						code="03"

					with open(filename) as f:
						binary += str(f.read().replace('\n', '\n'))
							#str(f.readlines())

					encoded = base64.b64encode(binary)

					msg = "Heartbeat: Filename: '"+filename+"' File: "+str(encoded)
					print "Heartbeat: Sending file "+str(filename)+"...."

				else:
					msg = "Heartbeat: "+str(sys.argv[1])
					print msg
				
				# Create the communication object and go!
				#                        statuscode  statusmsg,   transaction          requesttype     request   ):
				self.docomm(self.createrequest(code,msg,self.transaction,"report",self.createreport()))
			else:
				# Create the communication object and go!
				#                        statuscode  statusmsg,   transaction          requesttype     request   ):
				jsonobj = self.createrequest("01","Heartbeat",self.createtransaction(),"report",self.createreport())
				self.docomm(jsonobj)
	# END DEF


	def getConfig(self):
		return self.conf
	# END DEF

	def load_conf(self):
		self.hb_conf = str("")
	
		# conf file config
		hbconf = "/root/heartbeat/hb.conf"

		if not os.path.isfile(hbconf):
			print "Conf file does not exist: ", hbconf
			a = os.system("cp /root/heartbeat/default-hb.conf /root/heartbeat/hb.conf ")

		try:
			# Read in conf file
			fh = open("/root/heartbeat/hb.conf","r")
			self.hb_conf = fh.read()
			fh.close()
		except Expcetion, e:
			self.flag['no-conf'] = TRUE
			print 'The conf file could not be read. Please check the hb.conf file and try again.'
			exit(1)
		return json.loads(self.hb_conf)

	# END DEF

	def call_heartbeat(self,arg1):

		if self.transaction:
			a = os.system("python /root/heartbeat/heartbeat.py \""+arg1+"\" "+self.transaction)
		else:
			a = os.system("python /root/heartbeat/heartbeat.py \""+arg1+"\"")

	# END DEF

	def docomm(self,report):
	
		#if self.debug == True:
		#	print "Sending to server: ", report
		# Send the object to the server(s)
		# Show Variables
		
		self.threads = []

		for k,v in self.conf['servers'].items():
			if v['enabled'] == "true":
				print "Comm-Server: ", k, "\n"
				#tthread.start_new_thread(self.docommjob, (report,v,))
				thread = Thread(target=self.docommjob, args=(report,v,))
				thread.start()
				self.threads.append(thread)
			else:
				print "Server "+str(k)+" is not enabled.\n"
		for thread in self.threads:
			thread.join()		

	# END DEF

	def docommjob(self,report,server):
		response = self.talklisten(report,server)
		#print "Response from server: ", response
		
		if response:

			transaction = report['transaction']
			# Process what we got back from the server
			result = self.process_request(response,server)

			#print result

			# Create a new communication object to send to server as validation
			#report = self.createrequest("01",result,transaction,"response","")

			# Send the object to the server
			#self.docomm(report)
		else:
			print "Did not get a response from the server."
	# END DEF

	def talklisten(self,data,server):

		try:
			# Prepare out data...
			print '\n\n'+str(data)+'\n\n'
			encoded_device_data = json.dumps(data, encoding='utf8')
			data = zlib.compress(encoded_device_data, 8)
			data = {"data":encoded_device_data}
			params = urllib.urlencode(data)
			headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"} #, "Accept-encoding": "gzip"}
			try:
				if(server['protocol'] == "http"):
					http_request = httplib.HTTPConnection( server['url'], int(server['port']), timeout=int(server['timeout']) )
				elif(server['protocol'] == "https"):
					http_request = httplib.HTTPSConnection( server['url'], int(server['port']), timeout=int(server['timeout']) )

				http_request.request( "POST", "/heartbeat.php", params, headers )
				response = http_request.getresponse()
				response_data = response.read()
				http_request.close()
				return response_data
			except Exception, e:
				print "There was an error with HTTP/HTTPS: ", e
				return False
				
		except Exception, e:
			print "There was an error encoding data in self.talklisten: ", e	
			return False


	# END DEF

	def process_request(self,response_data,server):

		if self.debug == True:
			print "Debug Response Data: \n\n", response_data, "\n\n"

		try:
			if response_data:

				rd = json.loads(response_data)
				#print "Transaction: "+rd['transaction']
				transaction = rd['transaction']

				if rd['status']['message'] != None and rd['status']['message'] != "":
					msg = rd['status']['message']
					msg = str(msg)
				if rd['status']['code'] != None and rd['status']['code'] != "":
					code = rd['status']['code']
					code = str(code)

				'''
				if msg != None and msg != "":
					print "\n\n\tMsg: ", msg
					print "\n"

				if code != None and code != "":
					print "\n\n\tCode: ", code
					print "\n"
				'''


				# For some reason, this does not seem to loop properly, so it doesn't appear to ever run more than one command. Fix it. user fork,v dict.items() perhaps
				if rd['request']:
					
					self.commandthreads = [] # A place to load up the commands
					
					if(server['commands-allowed'] ==  "true"):

						for key in rd['request']:  # For each command...
							
							value = rd['request'][key]						
							print "Command Key: ", key, "Command Value: ", value, "\n\n"
							thread = Thread(target=self.docommandjob, args=(server,value,self.transaction,))
							self.commandthreads.append(thread)
							thread.start()

						for thread in self.commandthreads:
							thread.join()

					else:
						print "Not allowed to receive commands from "+str(server['url'])+"\n"
						self.call_heartbeat("Heartbeat: Not allowed to receive commands from "+str(server['url']))

					###
				###
			###

		except Exception, e:
			print "There was an error processing Heartbeat request: ", e, "\n\n" ,traceback.print_exc(), "\n\n"

	# END DEF


	def docommandjob(self,server,command,transaction):

		pcs = list()
		arg = str()

		try:
			print "Received: ", command
			pcs = command.split()
			pcslen = int(len(pcs))
		except Exception, e:
			self.call_heartbeat("Heartbeat: Could not split command: "+str(command))

		command = str(pcs[0])
		print "Command: ", command

		if(pcslen > 1):
			try:
				arg = str(pcs[1])
				print "ARG: ", arg
			except Exception, e:
				pass
				#print "No argument for command: ", e

		if(pcslen > 2):
			try:
				arg2 = str(pcs[2])
				print "ARG2: ", arg2
			except Exception, e:
				pass
				#print "No argument for command: ", e

		self.call_heartbeat("Heartbeat: Received Command: "+str(command)+" from "+str(server['url']))

		if command == "ReverseSSH":
			print '\tEstablishing Reverse SSH Connection to Heartbeat Server:'
			
			sshport = str() # We will put a port number in here
			
			if not arg:
				print "9956"
				sshport = "9956"
			else:
				print arg
				sshport = arg
			print '\n'

			if not os.path.isfile("/usr/bin/expect"):
				os.system(str(self.dir)+'/heartbeat.py "Heartbeat: expect not installed. Installing now..."')
				a = os.system("apt-get install expect -y")

			subprocess.call("cd "+str(self.dir)+"/commands && expect "+str(self.dir)+"/commands/reversessh.expect "+str(sshport),shell=True)
			
			return "Started ReverseSSH to Server"
		elif command == "speedtest-report":
                        a = os.system("bash ./commands/speedtest.bash &")
		elif command == "SyncPubKey":
			print '\tReceiving PubKey from ' + arg + '\n\n'
			decoded = base64.b64decode(arg2)
			print "\t\tDecoded: ", str(decoded)

			try:
				# Let's make sure the reverse user exists on the reverse ssh destination. If not, let's simply report to heartbeat that the user needs to be setup with link to page with more details.
				try:
					if pwd.getpwnam('reverse'):
						valid_ssh_user = True
				except KeyError:
					valid_ssh_user = False

				if valid_ssh_user == True:
					# Let's make sure the authorized_keys file exists
					authorizedkeysdirectory = "/home/reverse/.ssh/"
					authorizedkeysfile = "authorized_keys"
					authorizedkeyspathfile = authorizedkeysdirectory+authorizedkeysfile
					if not os.path.exists(authorizedkeysdirectory):
					    os.makedirs(authorizedkeysdirectory)
					
					# Now let's open up the authorized_keys file and remove the line that matches the machine with this same host name in "arg"
					a = os.system("sed -i '/root@"+arg+"/d' "+authorizedkeyspathfile)

					# Then we need append the decoded pub key to the file
					a = os.system("echo \""+decoded+"\" >> "+authorizedkeyspathfile+"\n")
					
					self.call_heartbeat("Public Key from "+arg+" was Sync'd.")
					return "Public Key Sync'd!"
				else:
					self.call_heartbeat("Public Key from " + arg + " was not Sync'd. Reverse SSH user does not exist.")
					return "Public Key was not Sync'd. Reverse SSH user does not exist."
			except Exception, e:
				self.call_heartbeat("An Exception Occured. Public Key from "+arg+" was not Sync'd: "+e)

		elif command == "SendFile":
			if not arg:
				print "There was an error: No File Requested"
				self.call_heartbeat("Exception: No file specified.")
			else:
				print '\tRequesting File:', arg
				self.call_heartbeat("file:"+str(arg))

			print '\n'

			return "File Sent"
		elif command == "GetFile":
			if not arg:
				print "There was an error: No File Requested"
				self.call_heartbeat("Exception: No file specified.")
			else:
				print '\tRequesting File:', arg
				self.call_heartbeat("file:"+str(arg))

			print '\n'

			return "File Sent"
		elif command == "PutFile":
			
			if not arg or not arg2:

				if not arg:
					print "There was an error: No Path or Filename Specified"
					self.call_heartbeat("Exception: No Path or Filename Specified. File not saved.")

				if not arg2:
					print "There was an error: No file Payload"
					self.call_heartbeat("Exception: No Path or Filename Specified. File not saved.")

			else:

				print '\t:Saving file', arg
				decoded = base64.b64decode(arg2)
				
				print "Decoded: ", str(decoded)
				
				f = open(arg, 'r+')
				f.seek(0)
				f.write(decoded)
				f.truncate()
				f.close()
				self.call_heartbeat("file:"+str(arg))

			print '\n'

			return "File Sent"
		elif command == "DisconnectReverseSSH":
			print "Shutting down the Reverse SSH connection if it exists\n"
			a = os.system("kill `pidof ssh` &")		
			return "Disconnected ReverseSSH"
		elif command == "UpdatePowrz": # Tell the device to update now
			print 'Update Powrz: ', command[7:]
			a = os.system("bash /root/powrz/scripts/check_update.bash &")	
			return "Started force update"
		elif command == "UpdateHeartbeat": # Update Heartbeat!
			print 'UpdateHeartbeat: '
			a = os.system("cd "+str(self.dir)+" && bash "+str(self.dir)+"/update.bash &")
			return "Started Heartbeat Update"
		elif command == "UpdateHeartbeatGit": # Update Heartbeat!
			print 'UpdateHeartbeat: '
			a = os.system("cd "+str(self.dir)+" && git reset --hard origin/master && git pull &")
			return "Started Heartbeat Update"
		elif command == "Heartbeat": # Update Heartbeat!
			print 'Heartbeat: '          
			a = os.system("bash ./commands/heartbeat.bash &")
			return "Ran NTPDate to sync system clock to NTP servers"
		elif command == "SyncNTP": # Update Heartbeat!
			print 'SyncNTP: '          
			a = os.system("ntpdate -bsu pool.ntp.org &")
			return "Ran NTPDate to sync system clock to NTP servers"
		elif command == "RestartNetworking": # Update Heartbeat!
			print 'RestartNetworking: '          
			a = os.system("service networking restart &")
			return "Restarted Networking"
		elif command == "RestartSSH": # Update Heartbeat!
			print 'RestartSSH: '          
			a = os.system("service ssh restart &")
			return "Restarted SSH"
		elif command == "RestartSSHd": # Restart SSHd!
			print 'Restart SSHd Service: '   
			a = os.system("service sshd restart &")
			return "SSHd Restarted"
		elif command == "Indication": # Indication                                                           
			print 'Running Indication: '
			a = os.system("bash ./commands/indication.bash &")
			return "Indication Running" 
		elif command == "Indication10": # Indication
			print 'Running Indication: '
			a = os.system("bash ./commands/indication.bash 10 &")
			return "Indication 10 Running"
		elif command == "GetCron": # Update Heartbeat!
			print 'Getting and sending Cron: '     
			a = os.system("bash ./commands/getcron.bash &")
		elif command == "WipeRootKnownHosts": # Update Heartbeat!
			print 'Wiping known_hosts for root: '
			a = os.system("echo "" > /root/.ssh/known_hosts &")
		elif command == "generate-sync-new-rsa":
			print 'Generating new RSA Key Pair'
			a = os.system("bash ./commands/generate-and-sync-new-rsa-key.bash "+self.transaction+"&")
                elif command == "pm2restart": # PM2 Restart
                        print 'Reloading PM2: '
                        a = os.system("bash ./commands/pm2restart.bash &")
                elif command == "pm2start": # PM2 Start
                        print 'Starting PM2: '
                        a = os.system("bash ./commands/pm2start.bash &")
                elif command == "pm2stop": # PM2 Stop
                        print 'Stopping PM2: '
                        a = os.system("bash ./commands/pm2stop.bash &")
                elif command == "gitresetpull": # Git reset --hard && git pull
                        print 'Resetting and Pulling from Gitlab: '
                        a = os.system("bash ./commands/gitresetpull.bash &")
		elif command == "Reboot": # Reboot the Device!
			print 'Reboot: '
			a = os.system("shutdown -r now")
			return "Made System Call"
		else:
			print "Unknown Command Received from Heartbeat: ", command
			return "Unknown Command Received from Heartbeat: " +str(command)
	# END DEF



	def createrequest(self,statuscode,statusmsg,transaction,requesttype,request):
			
			report = {}
			report['status'] = {}
			report['request'] = {}	# Set as dict

			report['timestamp'] = str(time())
			report['device'] = self.gethostname()
			report['transaction'] = transaction
			report['status']['code'] = statuscode
			report['status']['message'] = statusmsg
			
			report['requesttype'] = requesttype	# Set as dict
			report['request'] = request

			return report
	# END DEF


	def gethostname(self):
		return socket.gethostname()
	# END DEF


	def createreport(self):

		data = {}
		data['networking'] = ""
		data['program'] = ""
		data['cpuload'] = ""
		data['memory'] = ""
		data['diskuse'] = ""
		data['cpu_temp'] = ""
		data['state'] = self.state

		with open('/root/heartbeat/version.txt', 'r') as content_file:
			data['HeartbeatVersion'] = content_file.read().strip()

		# Get LocalIP
		try:
			lip = subprocess.check_output("hostname -I", shell=True)
			data['lip'] = lip.strip()
		except Exception, e:
			data['lip'] = ""
			print 'Could not get Local IP: ', e

		
		# Get WanIP
		try:
			p = subprocess.Popen(['dig','+short','myip.opendns.com','@resolver1.opendns.com', ''], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			wan, err = p.communicate()
			data['wip'] = wan.strip()
		except Exception, e:
			data['wip'] = ""
			print 'Could not get WAN IP Address: ', e


		# Get All Networking
		try:
			p = subprocess.Popen('/sbin/ifconfig', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['networking'] += str(line)
			retval = p.wait()
			del p
		except Exception, e:
			data['networking'] = ""
			print 'Could not get All Networking: ', e

		
		# Is main.py running?
		try:
			p = subprocess.Popen('ps aux | grep "main.py" | grep -v "grep"', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['program'] += str(line)
			retval = p.wait()
			del p
		except Exception,e:
			data['program'] = ""
			print "There was an error checking if main.py was running: ", e

		# system resources (CPU Lod Avg)
		try:
			p = subprocess.Popen('uptime', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['cpuload'] += str(line)
			retval = p.wait()
			del p
		except Exception,e:
			data['cpuload'] = ""
			print "There was an error getting CPU load: ", e

		# system resources (RAM)
		try:
			p = subprocess.Popen('free -m | grep Mem', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['memory'] += str(line)
			retval = p.wait()
			del p
		except Exception,e:
			data['memory'] = ""
			print "There was an error checking RAM usage: ", e

		# system resources (Disk Usage)
		try:
			p = subprocess.Popen('df -h', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['diskuse'] += line
			retval = p.wait()
			del p
		except Exception,e:
			data['diskuse'] = ""
			print "There was an error checking Disk Usage: ", e
		#print "\n\n", data, "\n\n"

		# Software Installed
		#try:
		#	data['diskuse']
		#except Exception,e:
		#	data['diskuse'] = ""
		#	print "There was an error checking Disk Usage: ", e
		#print "\n\n", data, "\n\n"


		# What software is installed on this device?
		data['software'] = {}
		try:
			if os.path.isfile("/root/powrz/backend/main.py"):
				data['software']['powrz'] = "Installed"
			
			if os.path.isfile("/root/application/ghc/main.py"):
				data['software']['ghc'] = "Installed"

			if os.path.isfile("/root/application/rrc/main.py"):
				data['software']['rrc'] = "Installed"

                        if os.path.isfile("/root/application/vader/main.py"):
                                data['software']['vader'] = "Installed"
		except Exception,e:
			data['software'] = ""
			print "There was an error checking Installed Software: ", e
		#print "\n\n", data, "\n\n"


		# CPU Temp - BB's only
		try:
			p = subprocess.Popen('expr `cat /sys/class/hwmon/hwmon0/device/temp1_input` / 1000 \* 9 / 5 + 32', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
			for line in p.stdout.readlines():
				data['cpu_temp'] += line
			retval = p.wait()
			del p
			data['cpu_temp'] = int(data['cpu_temp'])
			if data['cpu_temp']:
				data['cpu_temp'] = str(data['cpu_temp'])+"F"
			else:
				data['cpu_temp'] = str("N/A")
				
		except Exception,e:
			data['cpu_temp'] = ""
			print "There was a non-fatal error checking CPU Temp for BBB/BBG: ", e




		return data
	# END DEF


	def createtransaction(self):
		self.transaction = ''.join(random.choice('0123456789abcdef') for i in range(32))
		return self.transaction
	# END DEF

# END CLASS

if __name__ == "__main__":
	heartbeat = heartbeat()
