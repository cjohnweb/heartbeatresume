#!/bin/bash


# Add heartbeat to crontab
DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

# Save current Cron to file
crontab -l > $DIR/mycron5df0329idj

if grep -q "python $DIR/heartbeat.py" "$DIR/mycron5df0329idj"
then
  if grep -q "* * * * * sudo python $DIR/heartbeat.py" "$DIR/mycron5df0329idj"
  then
    # Pass, nothing to do
    echo -en "sudo Heartbeat found in Cron"
  else
		echo -en "Removing Heartbeat from Cron..."
    sed -i -e '/Heartbeat/d' $DIR/mycron5df0329idj
    sed -i -e '/heartbeat/d' $DIR/mycron5df0329idj                
    # Append new cron command
    echo -en "\n# Heartbeat\n* * * * * sudo python $DIR/heartbeat.py\n" >> "$DIR/mycron5df0329idj"
  fi
else
  # Append new cron command
  echo -en "\n# Heartbeat\n* * * * * sudo python $DIR/heartbeat.py\n" >> $DIR/mycron5df0329idj
fi

if grep -q "python $DIR/update.bash" "$DIR/mycron5df0329idj"
then
	echo -en "Removing Heartbeat update from Cron..."
	sed -i -e '/heartbeat\/update.bash/d' $DIR/mycron5df0329idj
fi

# Install new cron file
crontab $DIR/mycron5df0329idj
rm $DIR/mycron5df0329idj

if [ ! -f $DIR/init.hash ]; then
  echo "" > $DIR/init.hash
fi

hash="$(sha1sum $DIR/init.bash)"
lasthash="$(cat $DIR/init.hash)"

if [ "${lasthash}" != "${hash}" ]
then

	# Install Adafruit BBIO Library
	sudo apt-get update
	sudo apt-get install build-essential python-dev python-setuptools python-pip python-smbus -y
  sudo pip install Adafruit_BBIO

	# Upgrade pip
	pip install --upgrade pip

	# Install Report Device Power on
	#python $DIR/heartbeat.py "Notice: init.bash: Hash: Installing Boot Notifications"
	echo -en "#!/bin/sh\nFLAGFILE=/var/run/report-boot-via-heartbeat\ncase \"\" in\n\tlo)\n\t\t# The loopback interface does not count.\n\t\t# only run when some other interface comes up\n\t\texit 0\n\t\t;;\n\n\tusb0)\n\t\texit 0\n\t\t;;\n\t*)\n\t\t;;\nesac\n\nif [ -e  ]; then\n\texit 0\nelse\n\ttouch\n fi\npython $DIR/heartbeat.py \"Powered On\"" > /etc/network/if-up.d/report-boot.bash
	chmod +x /etc/network/if-up.d/report-boot.bash
	chmod 777 /etc/network/if-up.d/report-boot.bash
	chown root:root /etc/network/if-up.d/report-boot.bash
  echo "$hash" > $DIR/init.hash
  python $DIR/heartbeat.py "Notice: init.bash: Triggered: Finished Hash Code"
else
  python $DIR/heartbeat.py "Notice: init.bash: Triggered: Hashes Same, No Changes"
fi

python $DIR/heartbeat.py "Heartbeat: init.bash: Finished"