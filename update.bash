#!/bin/bash

DIR=$(pwd)
echo -en "\n\ncd $DIR \n\n"
cd $DIR

python $DIR/heartbeat.py "Updating Heartbeat via GIT"

#modified to remove sensitive user data
git remote set-url origin https://user:password@gitlab.com/user-or-group/repo.git

git reset --hard
git pull --force
realtimepid=$(ps aux | grep realtime.py | grep -v grep | awk '{print $2}')
kill $realtimepid
bash $DIR/init.bash
python $DIR/heartbeat.py "Heartbeat Updated via GIT Finished"