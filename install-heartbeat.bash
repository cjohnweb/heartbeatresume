#!/bin/bash

# Setup 

DIR=$( pwd )
echo -en "\n\ncd $DIR \n\n"
cd $DIR

cronfile="$DIR/cron9fh387h2e"
crontab -l > $cronfile
cronline="* * * * * python $DIR/heartbeat.py"

if grep -Fxq "$cronline" $cronfile
then
  echo "Heartbeat already in cron"
else
  # Append new cron command
  echo -en "\n# Heartbeat - Minute\n* * * * * sudo python $DIR/heartbeat.py\n" >> $cronfile
fi

# Install new cron file
crontab $cronfile
rm $cronfile

# Install dependancies
apt-get update
dpkg --configure -a 
apt-get upgrade -y
dpkg --configure -a
apt-get install expect dnsutils mtr -y

cd $DIR
python $DIR/heartbeat.py "Heartbeat: $DIR/install-heartbeat.py"
