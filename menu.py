#!/usr/bin/python
# encoding=utf8

import npyscreen

# read in the hb.conf file to self.conf dict
# change the menus according to the settings file
# create a route to validate device identity, and give an option to verify device identity




# This is a form object
class MainForm(npyscreen.Form):
  def create(self):
    self.button1 = self.add(npyscreen.Button, name="Authentication", value_changed_callback=self.goToAuthForm) # Make this button go to FORM2
    self.button2 = self.add(npyscreen.Button, name="Servers", value_changed_callback=self.goToServersForm) # Make this button go to FORM2
    self.button3 = self.add(npyscreen.Button, name="Settings", value_changed_callback=self.goToSettingsForm) # Make this button go to FORM2
    self.button4 = self.add(npyscreen.Button, name="Exit", value_changed_callback=self.exit) # Make this button go to FORM2

    # Since we are inheriting the npyscreen.FormWithMenus class we can use menus, this will add an option to the menu to exit the program
    #self.menu = self.new_menu(name="Main Menu", shortcut='^M')
    #self.menu.addItem("Exit Program", self.exit, "^X")
  # END DEF

  def goToAuthForm(self, widget):
    #npyscreen.notify_confirm("BUTTON PRESSED!", title="Woot!", wrap=True, wide=True, editw=1)
    self.parentApp.switchForm('AUTH')

  def goToServersForm(self, widget):
    #npyscreen.notify_confirm("BUTTON PRESSED!", title="Woot!", wrap=True, wide=True, editw=1)
    self.parentApp.switchForm('AUTH')

  def goToSettingsForm(self, widget):
    #npyscreen.notify_confirm("BUTTON PRESSED!", title="Woot!", wrap=True, wide=True, editw=1)
    self.parentApp.switchForm('AUTH')

  def exit(self):
    self.parentApp.switchForm(None) # causes the app to exit on OK
  # END DEF

  # Save data to conf file and Go back to first form...
  #def on_ok(self):
    #npyscreen.notify_confirm("OK Pressed, going to FORM2 now.", title="Notice", wrap=True, wide=True, editw=1)
    #self.parentApp.setNextForm('AUTH')
  # END DEF

  #def on_cancel(self):
    #npyscreen.notify_confirm("self.button:  "+str(self.button.__dict__.keys()), title="Notice", wrap=True, wide=True, editw=1)
    #self.parentApp.setNextForm(None) # Also exit's the program
  # END DEF

# END CLASS

# Authenticaiton Form
class AuthForm(npyscreen.ActionForm, npyscreen.SplitForm, npyscreen.FormWithMenus):
  def create(self):

    self.name = self.add( npyscreen.TitleText, name="Username: " )
    self.passwd = self.add( npyscreen.TitleText, name="Password: " )

  # Save data to conf file and Go back to first form...
  def on_ok(self):
    npyscreen.notify_confirm("Saved! Going back to main form", title="OK Presed", wrap=True, wide=True, editw=1)
    self.parentApp.setNextForm('MAIN')
  # END DEF

  def on_cancel(self):
    npyscreen.notify_confirm("NOT Saved, going back to main form", title="OK Presed", wrap=True, wide=True, editw=1)
    self.parentApp.setNextForm('MAIN') # Back to main form
  # END DEF
# END CLASS

# This is the Wizards form manager function
class WizardApp(npyscreen.NPSAppManaged):
  def onStart(self):
    self.addForm('MAIN', MainForm, name = "First Form!", lines=20, columns=60, draw_line_at=16 )
    self.addForm('AUTH', AuthForm, name = "Second Form!", lines=20, columns=60, draw_line_at=16 )
  # END DEF
# END CLASS


if ( __name__ == "__main__"):
  wizard = WizardApp().run()
